<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->


> Please note that this **is not** a usual Git repository.
> It demonstrates every important step performed in the workshop:
>   - Branch `00-intial-version` shows the initial version of the software without any tests.
>   - Branch `01-add-further-characterization-tests` is the basis for adding initial characterization tests.
>   - Branch `02-add-further-mocked-parser-tests` is the basis to get started with the `mock` library.
>   - Branch `03-check-code-coverage` is the basis for the analysis of the code coverage.
>   - Branch `04-improve-test-suite-structure` is the basis to play around with the different test types.
>   - Branch `05-add-build-automation` is the basis for demonstrating build automation aspects and the integration with GitLab.


# HTML Parser

html-parser is a command line tool to extract links from HTML documents (e.g., from local files or URLs).

## User Guide

### Installation

At least you require Python >= 3.9 to use the tool.
To install it, download and extract the source code archive and perform the following steps:

```
cd html-parser-1.0.0
python setup.py install
```

### Usage

Run the tool as follows:

```
> export PYTHONPATH=.
> python -m html_parser.main <URL_or_LOCAL_HTML_FILE>
```

For more information, please see the help message:

```
General Usage:

html-parser [PATH TO HTML DOCUMENT]
It currently parses a HTML document and prints all links.

Example local file:   html-parser /home/user/test.html
Example HTTP:         html-parser https://www.google.de/
```

## Developer Guide

### Getting Started

We assume that you have installed the following tools:

- [Python >= 3.9](https://www.python.org/downloads/)
- [Poetry >= 1.6](https://python-poetry.org/docs/#installation)
- [Git command line client](https://git-scm.com/downloads)
- A bash shell including GNU make (>=4.3)

**1. Checkout the latest source code from the repository**

`git clone <GIT URL>`


**2. Install the development dependencies**

```
cd html-parser
poetry install
```

**3. Check that everything works fine**

`make audit`

**4. Start with your work :-)**


### Available Build Targets

In general, you invoke the build script as follows:

``make <BUILD TARGET NAME>``


The following build targets are available:

- **clean** - Cleans up temporary created directories and files.
- **tests** - Executes the tests.
  The results are shown on the command-line.
  In addition, the results are made available as XML report for the build pipeline (in ``build/tests.xml``).
- **tests-quick** - Executes only the small and medium tests.
- **formatting** - Formats the code using ``black``.
- **audit** - Performs all relevant code checks. The build target will succeed, if:
  - ``flake8`` and ``black`` issue no problems.
  - All tests run successfully.
  - The combined code coverage of small and medium tests is greater than 60%.
  - The code coverage of all tests is greater than 90% (coverage report is in ``build/htmlcov``).
  - Check that sufficient license information is provided.
- **package** - Creates an installable release package.
  The package is available in the ``dist`` directory.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
